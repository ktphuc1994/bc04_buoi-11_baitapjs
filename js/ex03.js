// BÀI 3: Quy đổi tiền
/**
 * INPUT
 * exUSDVND = 23500;
 * usdAmount = 999;
 *
 * TODO
 * vndAmount = exUSDVND * usdAmount;
 *
 * OUTPUT
 * vndAmount = 23476500;
 */

var exUSDVND, usdAmount, vndAmount;
exUSDVND = 23500;
usdAmount = 999;

vndAmount = exUSDVND * usdAmount;

console.log("vndAmount: ", vndAmount);
