// BÀI 4: Tính diện tích, chu vi hình chữ nhật
/**
 * INPUT
 * width = 37.5;
 * height = 11.9
 *
 * TODO
 * recArea = width * height;
 * recPerimeter = (width + height) * 2;
 *
 * OUTPUT
 * recArea = 446.25;
 * recPerimeter = 98.8;
 */

var width, height, recArea, recPerimeter;
width = 37.5;
height = 11.9;

recArea = width * height;
recPerimeter = (width + height) * 2;

console.log("recArea: ", recArea);
console.log("recPerimeter: ", recPerimeter);
