// BÀI 5: Tính tổng 2 ký số
/**
 * INPUT
 * n = 97;
 *
 * TODO
 * dozen = Math.floor(n / 10);
 * unit = n % 10;
 *
 * OUTPUT
 * sumNumber = 9 + 7 = 16;
 */

var n, dozen, unit, sumNumber;
n = 97;

dozen = Math.floor(n / 10);
unit = n % 10;

sumNumber = dozen + unit;

console.log("sumNumber: ", sumNumber);
