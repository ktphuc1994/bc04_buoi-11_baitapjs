// BÀI 2: Tính giá trị trung bình
/**
 * INPUT
 * 5 số thực từ 1 đến 5:
 * n1 = 14.16;
 * n2 = 17;
 * n3 = 146.7;
 * n4 = -19.5;
 * n5 = pi; (số pi)
 *
 * TODO
 * averageN = (n1 + n2 + n3 + n4 + n5) / 5;
 *
 * OUTPUT
 * averageN = 32.30031853071795;
 */

var n1, n2, n3, n4, n5, averageN;
n1 = 14.16;
n2 = 17;
n3 = 146.7;
n4 = -19.5;
n5 = Math.PI;

averageN = (n1 + n2 + n3 + n4 + n5) / 5;

console.log("averageN: ", averageN);
