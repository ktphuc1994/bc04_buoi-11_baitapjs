// BÀI 1: Tính tiền lương nhân viên
/**
 * INPUT:
 * dailyPay = 100000 (lương 1 ngày);
 * workDay = 21 (số ngày làm việc);
 *
 * TODO:
 * salary = dailypay * workday;
 *
 * OUTPUT:
 * salary = 2100000;
 */

var dailyPay = 100000;
var workDay = 21;
var salary = null;

salary = dailyPay * workDay;

console.log("salary: ", salary);
